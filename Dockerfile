FROM ubuntu:latest
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update && apt-get install --no-install-recommends -y texlive-full make
